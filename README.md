#  Weather noticer  天气布告栏
## 功能
按照设置的定时条件，上午7点拉取一次天气信息，下午7点拉取一次。分别发送给设定的手机号码。

如果要运行程序，需要将`config_demo.yaml`更改为`config.yaml`。并将其中的相关数据替换为自己的数据。
本程序使用的短信服务提供商为云片。
