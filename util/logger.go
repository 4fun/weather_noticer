package util

import (
	"time"

	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var Log *logrus.Logger

func NewLogger() *logrus.Logger {
	if Log != nil {
		return Log
	}
	path := viper.GetString("log.path")
	if path == "" {
		path = "./log/default"
	}
	writer, err := rotatelogs.New(
		path+".%Y%m%d",
		// rotatelogs.WithLinkName(path),
		rotatelogs.WithRotationTime(time.Hour*24),
		rotatelogs.WithRotationCount(10),
	)
	CheckError(err)
	lfsHook := lfshook.NewHook(
		lfshook.WriterMap{
			logrus.InfoLevel:  writer,
			logrus.ErrorLevel: writer,
		},
		&logrus.JSONFormatter{},
	)
	Log := logrus.New()
	Log.Hooks.Add(lfsHook)
	return Log
}
