package util

import (
	"time"
)

var timeLayout = "2006-01-02 15:04:05"

// ParseTime 解析字符串，返回字符串所代表的时间，返回格式为timeLayout格式。
func ParseTime(t string) time.Time {
	tt, err := time.ParseInLocation(timeLayout, t, time.Local)
	CheckError(err)
	return tt
}
