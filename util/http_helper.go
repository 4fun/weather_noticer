package util

import (
	"io/ioutil"
	"net/http"
	"net/url"
)

// PostForm 使用POST对指定的url进行请求，并以URL_Encode处理后的键值对作为请求体。最后返回请求后的数据。
func PostForm(url string, data url.Values) []byte {
	resp, err := http.PostForm(url, data)
	CheckError(err)
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	CheckError(err)
	return body
}

// HTTPGet 使用特定的Header信息去请求指定的url，并返回相应的额响应和错误。
func HTTPGet(url string) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36")

	return http.DefaultClient.Do(req)
}
