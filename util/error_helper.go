package util

import (
	"log"
)

// 如果err不为空，FatalError将调用os.Exit(1)
func CheckError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
