package util

// Substring returns the substring of src from index start to end.
func Substring(src string, start, end int) string {
	len := len(src)
	if start < 0 || end > len || start > end {
		return ""
	}
	if start == 0 && end == len {
		return src
	}
	return string(src[start:end])
}

// Substring returns the substring of src from index start to the real end of src.
func SubstringStart(src string, start int) string {
	return Substring(src, start, len(src))
}
