package config

import (
	"fmt"
	"sync"

	"github.com/spf13/viper"
)

var once = &sync.Once{}

func Init() {
	once.Do(func() {
		viper.SetConfigName("config")
		viper.AddConfigPath("./config")
		err := viper.ReadInConfig()
		if err != nil {
			fmt.Errorf("读取配置文件失败：\n\r%v", err)
		}
	})
}
