package service

import (
	"encoding/json"
	"io/ioutil"
	"net/url"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"gitea.com/4fun/weather_noticer/model"
	"gitea.com/4fun/weather_noticer/util"
)

// GetWeather返回API返回的天气信息
func GetWeather() (model.Weather, bool) {
	// 读取配置
	link := viper.GetString("weather.link")
	city := viper.GetString("weather.city")
	// 发送请求
	data, err := Get(link+city, nil)
	util.CheckError(err)
	// 解析json数据
	var weather model.Weather
	err = json.Unmarshal(data, &weather)
	util.CheckError(err)
	if weather.Status == 200 {
		return weather, true
	} else {
		return weather, false
	}
}

// Get返回给定apiURL的相应
func Get(apiURL string, params url.Values) (rs []byte, err error) {
	url, err := url.Parse(apiURL)
	if err != nil {
		logger.WithFields(logrus.Fields{
			"err": err,
		}).Error("解析URL错误")
		return nil, err
	}
	// 如果参数中含有中文，下面的方法会进行URLEncode
	url.RawQuery = params.Encode()
	resp, err := util.HTTPGet(url.String())
	if err != nil {
		logger.WithFields(logrus.Fields{
			"err": err,
		}).Error("HTTP请求错误")
		return nil, err
	}
	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}
