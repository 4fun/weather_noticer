package service

import (
	"net/url"
	"strconv"
	"strings"

	"gitea.com/4fun/weather_noticer/model"
	"gitea.com/4fun/weather_noticer/util"
)

var logger = util.NewLogger()

// short message service
type Message struct {
	ApiKey        string
	Mobile        string
	TemplateId    string
	SmsLink       string
	TemplateValue model.TemplateValue
}

func (m *Message) Send(weather model.Weather, whichDay model.WhichDay) {
	// ******设置参数****** //
	tmplV := model.TemplateValue{}
	// 获取日期
	timeDesired := util.ParseTime(weather.Time)
	// 如果需要明天的天气，则获取明天的日期
	if whichDay == model.Tomorrow {
		timeDesired = timeDesired.AddDate(0, 0, 1)
	}
	year, month, day := timeDesired.Date()
	monthStr := strconv.Itoa(int(month))
	if int(month) < 10 {
		monthStr = "0" + monthStr
	}
	tmplV.Date = strings.Join([]string{strconv.Itoa(year), monthStr, strconv.Itoa(day)}, "-")
	// 获取需求日期的天气数据
	var fc model.ForeCastOneday
	for _, v := range weather.Data.Forecast {
		if v.Ymd == tmplV.Date {
			fc = v
		}
	}
	// 获取白天和夜晚天气数据
	tmplV.DayOfWeek = fc.Week
	tmplV.City = weather.Cityinfo.City
	tmplV.WeatherDay = fc.Day.Weather
	tmplV.WeatherNight = fc.Night.Weather
	tmplV.WindDay = fc.Day.Winddirection + " " + fc.Day.Windpower
	tmplV.WindNight = fc.Night.Winddirection + " " + fc.Night.Windpower
	tmplV.TemperatureHigh = util.SubstringStart(fc.High, strings.LastIndex(fc.High, " ")+1)
	tmplV.TemperatureLow = util.SubstringStart(fc.Low, strings.LastIndex(fc.Low, " ")+1)
	if whichDay == model.Today {
		tmplV.WhichDay = "今日"
		// 获取生活指数数据
		var indexDes string
		for _, v := range weather.Data.Indexes {
			if v.Name == "雨伞指数" {
				indexDes = v.Description
			}
		}
		tmplV.Air = "空气质量" + weather.Data.Air.Quality + "。" + indexDes
	} else {
		tmplV.Air = ""
		tmplV.WhichDay = "明日"
	}
	// 设置参数
	postValues := m.SetParams(tmplV)
	// 发起发送短信请求
	body := util.PostForm(m.SmsLink, postValues)
	// 打印反馈
	logger.Info(string(body))
}

// 设置TemplateValue属性，并返回POST的请求参数
func (m *Message) SetParams(t model.TemplateValue) url.Values {
	v := url.Values{}
	v.Set("apikey", m.ApiKey)
	v.Set("mobile", m.Mobile)
	v.Set("tpl_id", m.TemplateId)
	v.Set("tpl_value", url.Values{
		"#whichDay#":     {t.WhichDay},
		"#date#":         {t.Date},
		"#dayOfWeek#":    {t.DayOfWeek},
		"#city#":         {t.City},
		"#weatherDay#":   {t.WeatherDay},
		"#windDay#":      {t.WindDay},
		"#weatherNight#": {t.WeatherNight},
		"#windNight#":    {t.WindNight},
		"#tempH#":        {t.TemperatureHigh},
		"#tempL#":        {t.TemperatureLow},
		"#air#":          {t.Air},
	}.Encode())
	return v
}
