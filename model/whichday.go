package model

type WhichDay int

const (
	Today = iota
	Tomorrow
)
