package model

// 云片短信服务的短信模板模型
type TemplateValue struct {
	WhichDay        string
	Date            string
	DayOfWeek       string
	City            string
	WeatherDay      string
	WeatherNight    string
	WindDay         string
	WindNight       string
	TemperatureHigh string
	TemperatureLow  string
	Air             string
}
