package model

// Weather 是天气模型
type Weather struct {
	Message  string   `json:"message"`
	Status   int      `json:"status"`
	Date     string   `json:"date"`
	Time     string   `json:"time"`
	Cityinfo CityInfo `json:"cityInfo"`
	Data     Data     `json:"data"`
}

// CityInfo 是城市信息模型
type CityInfo struct {
	City       string
	CityKey    string
	Parent     string
	UpdateTime string
}

// Data 是具体天气数据模型
type Data struct {
	Air       Air              `json:"air"`
	Expand    Expand           `json:"expand"`
	Indexes   []Index          `json:"indexes"`
	Forecast  []ForeCastOneday `json:"forecast"`
	Yesterday ForeCastOneday   `json:"yesterday"`
	Hour24    []Hour           `json:"hour24"`
}

// Air 是空气质量数据模型
type Air struct {
	No2     string
	Mp      string
	Pm25    string
	O3      string
	Aqi     string
	Pm10    string
	Suggest string
	Time    string
	Co      string
	Quality string
}

// Expand 是扩展模型
type Expand struct {
	Humidity      string
	Weather       string
	Weathertype   int
	Updatetime    string
	Stemp         string
	Windpower     string
	Winddirection string
}

// Index 是生活指数模型
type Index struct {
	Icon        string
	Value       string
	Name        string
	Tips        string
	Description string
}

// ForeCastOneday 是未来某一天或昨天的天气预报模型
type ForeCastOneday struct {
	Date    string
	High    string
	Low     string
	Ymd     string
	Week    string
	Sunrise string
	Sunset  string
	Aqi     int
	Day     DayAndNight
	Night   DayAndNight
}

// DayAndNight 是未来天气的白天和夜间天气模型
type DayAndNight struct {
	Windpower     string
	Winddirection string
	Notice        string
	Weather       string
}

// Hour 是未来某个小时的天气模型
type Hour struct {
	Temperature   int
	Humidity      string
	Windpower     string
	Winddirection string
	Weather       string
	Weathertype   int
	Time          string
}
