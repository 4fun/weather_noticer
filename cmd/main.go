package main

import (
	"github.com/robfig/cron"
	"github.com/spf13/viper"

	"gitea.com/4fun/weather_noticer/config"
	"gitea.com/4fun/weather_noticer/model"
	"gitea.com/4fun/weather_noticer/service"
	"gitea.com/4fun/weather_noticer/util"
)

func main() {
	// 初始化配置
	config.Init()
	logger := util.NewLogger()
	// 新建短消息对象
	m := service.Message{}
	// 获取配置并设置消息内容
	m.Mobile = viper.GetString("sms.mobile")
	m.ApiKey = viper.GetString("sms.apikey")
	m.TemplateId = viper.GetString("sms.tmplid")
	m.SmsLink = viper.GetString("sms.url_tpl_sms") // 获取单发模板
	// 测试
	test := viper.GetBool("test.mode")
	if test {
		// **************测试模式**************
		logger.Info("******测试模式******")
		weather, ok := service.GetWeather()
		if ok {
			logger.Info("天气数据获取成功。")
			// m.Send(weather, model.Today)
			// m.Send(weather, model.Tomorrow)
		} else {
			logger.Info("解析错误: ", weather)
		}
	} else {
		// **************正常模式**************
		logger.Info("******正常模式******")
		logger.Info("天气布告栏开始运行...")
		// 设置定时器
		c := cron.New()
		// 获取定时模式
		modeToday := viper.GetString("cron.modeToday")
		modeTomorrow := viper.GetString("cron.modeTomorrow")
		// 今日天气推送
		err := c.AddFunc(modeToday, func() {
			weather, ok := service.GetWeather()
			if ok {
				logger.Info("今日天气数据获取成功。")
				m.Send(weather, model.Today)
				// times++
			} else {
				logger.Info("解析错误: ", weather)
			}
		})
		util.CheckError(err)
		// 明日天气推送
		err = c.AddFunc(modeTomorrow, func() {
			weather, ok := service.GetWeather()
			if ok {
				logger.Info("明日天气数据获取成功。")
				m.Send(weather, model.Tomorrow)
				// times++
			} else {
				logger.Info("解析错误: ", weather)
			}
		})
		util.CheckError(err)
		c.Start()
		// 阻塞线程
		select {}
	}
}
